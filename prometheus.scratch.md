

## Prometheus-server

### Getting
```bash
mkdir -p /host_data/prometheus
cd /host_data/prometheus
wget -q \
	https://github.com/prometheus/prometheus/releases/download/v2.14.0/prometheus-2.14.0.linux-amd64.tar.gz
tar -zxvf prometheus-2.14.0.linux-amd64.tar.gz
```

### Start
```bash
cd /host_data/prometheus/prometheus-2.14.0.linux-amd64
./prometheus --config.file=prometheus.yml
```

## node-exporter
```bash
cd /host_data/prometheus/
wget -q https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz
tar -zxvf ...
cd ...
./node_exporter
```




## Various links
* https://github.com/dotless-de/vagrant-vbguest/issues/193
